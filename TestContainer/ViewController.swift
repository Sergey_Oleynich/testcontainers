//
//  ViewController.swift
//  TestContainer
//
//  Created by Sergey Oleinich on 7/19/18.
//  Copyright © 2018 Sergey. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet fileprivate weak var container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //In this case autolayout CANNOT automatically calculate the size of UINavigationController
        let viewController = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Black"))
        
        //In this case autolayout can automatically calculate the size of ViewController
        //let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Black")
        
        addChildViewController(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(viewController.view)
        
        NSLayoutConstraint.activate([
            container.trailingAnchor.constraint(equalTo: viewController.view.trailingAnchor, constant: 0),
            container.topAnchor.constraint(equalTo: viewController.view.topAnchor, constant: 0),
            container.leadingAnchor.constraint(equalTo: viewController.view.leadingAnchor, constant: 0),
            container.bottomAnchor.constraint(equalTo: viewController.view.bottomAnchor, constant: 0)
            ])
        
        viewController.didMove(toParentViewController: self)
        
    }


}

